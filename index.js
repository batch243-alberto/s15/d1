// console.log("Hello World");

// [Section] Syntax, Statements and comments
// Statements in programming are instructions that we tell the computer to perform
// JS statements usually end with semicolon(;) (for best practices)
// Semicolons are not required in JS, but we will use it to help us train to locate where a statements end.
// A syntax in programming, it is the set of rules that we describes how statements must be constructed.
// All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in certain manner.

// Comments are parts of the code that gets ignored by the language 
// Comments are meant to describe the written code

/*
	There are two types of comments 
	1. Single-line comment denoted by two slashes
	2. Multi-line comment denoted by slash and asterisk
*/

// [Section ] Variables

// Variables are use to contain date
// Any information that is used by an application is stored in what we call the "memory".
// When we create variables, certain portions of a device's memory is given a name that we call variables.

// This makes it easier for us to associate information stored in our devices to actual "names " about information.

// Declaring variables
// Declaring variables - tells our devices that a variable name is created and is ready to store data.
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was not defined
	// Syntax 
		// let / const variableName;
let myVariable = "Ada Lovelace";
// const myVariable = "Ada Lovelace";
// console.log() is useful for printing values of variables or certain results of code into the Google Chrome browser's console
// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code. 
console.log(myVariable);

/*
	Guides in writing variables:
		1. Use the let keyword followed the variable name of your choose and use the assignment operator(=) to assigned value
		2. Variable name should start with lowecase character, use camelCase for multiple words. 
		3. For constant variables, use the 'const' keyword. 
		 	Using let: we can change the value of the variable 
		 	Using const: we cannot change the value of the variable
		4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion. 
*/

// Declare and initialize variables 
//  Initializing variable - the instance when a variable is given it's initial/ starting value 
	// Syntax
		// let/const variableName = value; 
let productName = "desktop computer"; 

console.log(productName);

let productPrice = 18999;
console.log(productPrice);

	// In the context of certain application, some variables/ information are constant and should not be changed. 
	// In this example, the interest rate for a loan, savng account or a mortgage must not be change due to real world concerns.

const interest =3.539;
console.log(interest);


// Reassigning variable values
// Reassigning a variable means changing it's initial or previous value into another value.
	// Syntax
		// variableNae = newValue;

productName = "laptop";
console.log(productName);
console.log(productName);
	
// let productName = "laptop";
// console.log(productName);
// this will not work because you can only declared using let once 


	// let variable cannot be re-declared within its scope.

// Values of constants cannot be changed and will simply return an error
// interest = 4.489;
// console.log(interest)

// Reassigning variables and initializing variables

let supplier; 
// Initialization
supplier ="Zuitt store";
// Reassignment
supplier = "Zuitt merch";

// const name;
// name = "George Alfred Cabaccang";
// console.log(name);
// For const initialze and declare should be present

const name ="George Alfred Cabaccang";

// var vs let/const
	// some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see var
	// var - is also used in declaring variable. but var is an EcmaScript1 feature [ES1 (Javascript 1997)]
	// let/const was introduced as a new feature in ES6 (2016)

	// What makes let/const different var?

a=5;
console.log(a)
var a;

// b = 6;
// console.log(b)
// let b;
// this is an error

	// let/const local/global scope
	// Scope essentially means where these variables are available for use
	// let and const are block scope
	// A block is a chunk of code bounded by{}. A block in curly braces
	// Anything within the curly braces is a block.

	let outerVariable = "hello from the other side";
	// Global scope is also accessible inside the chunks/ curly braces

	{
		let innerVariable = "hello from the block";
		console.log(innerVariable);
		console.log(outerVariable);
	}

	{
		let innerVariable = "hello from the second block";
		console.log(innerVariable);
		let outerVariable = "hello from the second block";
		// overide but still working
	}

	console.log(outerVariable);
	// console.log(innerVariable); this is an error because the console is outside the curly braces

// Multiple variable declarations
// Multiple variables can be declared in one line. 

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);
console.log(productCode);
console.log(productBrand);

// Using a variable with a reserved keyword will be an error

// const let = "hello";
// console.log(let);


//[Sections] Data Types 
// Strings - are series of characters that create a word, a phrase, a sentence or anything related to creating text. 
// Strings in JavaScript can be written using either single (') and double quote (").
// In other programing languages, only the double can be used for creating strings

let country ="Philippines";
let province ='Metro Manila';

// Concatenate
// Multiple string values can be combined to create a single string using the "+" symbol.

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = 'I live in the' + country;
console.log(greeting);

// The escape characters (\) in strings in combination with other characters can produce different effects.

// "\n" refers to creating a new line between text 
let mailAddress = "Metro manila\nPhilippines";
console.log(mailAddress); 

let message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/whole numbers 
// Difference in color outputs when consoled
let count ="26";
let headcount = 26;
console.log(headcount);
console.log(count);

// Decimal Numbers / Fractions
let grade = 98.7;
console.log(grade);

// exponential Notation (it will read as a number)
let planetDistance = 2e10;
console.log(planetDistance);

// the number become string
console.log("John's grade last quarter is " + grade);

// it wil not add but concatenate
console.log(count + headcount);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussion about creating logic to make our applucation respond to certain scenatios

let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);

// It will also make boolean a string
console.log("isMarried " + isMarried);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values.
// Arrays can store different data types but is normally use to store similar data types.

	// similar data types 
		// Syntax 
		// let/const arrayName = [elementA, elementB, elementC . . .]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades[1]);
console.log(grades);

// different data types
// Storing different data types inside an array is not recommended because it will not makes sense in the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create a complex data that contains pieces of information that are relevant to each other. 

// Syntax: 
	// let/const objectName = {
	// propertyA: value,
	// propertyB: value;
	// }

let person ={
	fullName: "Juan Dela Cruz",
	age:35,
	isMarried: false,
	contact: ["09171234567", "8123 4567"],
	address:{
		houseNumber: "345",
		city: "Manila"
	}
};

console.log(person);
console.log(person.contact[1]);
console.log(person.address.city);

// typeof operator is used to determine the type of data or the value of a variable.

console.log(typeof person);
// Note: Arrays is a special type of object with methods and functions to manipulate it. 
console.log(typeof details);

/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */
    const anime = ["one piece", "one punch man", "attack on titans"];
    // anime = ["kimetsu no yaiba"]; This is an error
    console.log(anime);
    anime[0] = "kimetsu no yaiba";
    console.log(anime);
    anime[5] = "dxd";
    console.log(anime);
    console.log(anime[4]); 

// Null
// It is used to intentionally expressed the absence of a value in a variable/ initialization.
// Null simply meants that a data type was assigned to a variable but it does not hold any value/amount or is nullified
let spouse = null;
console.log(spouse);
// Undefined
// Represents the state of a variable that has been declared but without value
let fullName;
console.log(fullName);


// undefined vs null
// The difference between undefined and nulls is that for undefined , a variables was created but not provided a value.
// null means that a variable was created and was assigned a value  that does not hold any value/ amount.



